import { PinoLogger } from 'nestjs-pino'
import { Controller } from '@nestjs/common'
import { GrpcMethod } from '@nestjs/microservices'
import { MailerService } from '@nestjs-modules/mailer'
import { SendMailPayload, SendMailInput, MailTemplate } from 'src/_proto_/ts/mailer';

@Controller()
export class MailerController {
  constructor(private readonly service: MailerService, private readonly logger: PinoLogger) {
    logger.setContext(MailerController.name)
  }

  @GrpcMethod('MailerService', 'send')
  async send(input: SendMailInput): Promise<SendMailPayload> {
    const mailInput = {
      ...input,
      data: JSON.parse(Buffer.from(input.data).toString())
    }

    this.logger.info('MailerController#send.call %o', mailInput)

    let subject = ''
    let template = ''


    switch (mailInput.template) {
      case MailTemplate.SIGNUP:
        subject = 'Welcome'
        template = 'Welcome'
        break
      case  MailTemplate.UPDATE_EMAIL:
        subject = 'Notice: Email Update'
        template = 'update-email'
        break
      case MailTemplate.UPDATE_PASSWORD:
        subject = 'Notice: Password Update'
        template = 'update-password'
        break
      case MailTemplate.RECOVERY_PASSWORD:
        subject = 'Notice: Password recovery'
        template = 'Welcome'
        break
      default:
        break
    }

    await this.service.sendMail({
      to: mailInput.to,
      subject,
      template: template,
      context: mailInput.data
    })

    this.logger.info('MailerController#sent')

    return { isSent: true }
  }
}
