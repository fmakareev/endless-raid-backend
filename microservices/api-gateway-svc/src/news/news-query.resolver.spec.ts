import { Test, TestingModule } from '@nestjs/testing';
import { NewsQueryResolver } from './news-query.resolver';

describe('NewsQueryResolver', () => {
  let resolver: NewsQueryResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NewsQueryResolver],
    }).compile();

    resolver = module.get<NewsQueryResolver>(NewsQueryResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
