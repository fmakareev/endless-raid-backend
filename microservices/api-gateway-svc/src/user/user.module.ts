import { Module, forwardRef } from '@nestjs/common';
import { USER_PACKAGE_NAME, protobufPackage } from '../_proto_/ts/user';
import { ConfigService } from '@nestjs/config';
import { ClientGrpcProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { AuthModule } from '../auth/auth.module';
import { PasswordModule } from '@app/common';
import { UserMutationResolver } from './user-mutation.resolver';
import { UserQueryResolver } from './user-query.resolver';

@Module({
  imports: [
    PasswordModule,
    forwardRef(() => AuthModule),
  ],

  providers: [
    {
      provide: USER_PACKAGE_NAME,
      useFactory: (configService: ConfigService): ClientGrpcProxy => {
        return ClientProxyFactory.create({
          transport: Transport.GRPC,
          options: {
            url: configService.get<string>('USER_SVC_URL'),
            package: protobufPackage,
            protoPath: join(__dirname, '/_proto_/user.proto'),
          },
        });
      },
      inject: [ConfigService],
    },
    UserMutationResolver,
    UserQueryResolver,
  ],
  exports: [USER_PACKAGE_NAME],
})
export class UserModule {
}
