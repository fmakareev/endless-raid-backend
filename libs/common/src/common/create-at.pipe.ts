import {Injectable, PipeTransform} from '@nestjs/common';
import {Metadata} from "grpc";


@Injectable()
export class CreateAtPipe implements PipeTransform {
    transform(value: any) {
        if (value instanceof Metadata) {
            return value;
        }
        return {
            ...value,
            createAt: new Date().toISOString(),
        };
    }
}
