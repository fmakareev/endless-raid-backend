import { Args, Query, Resolver } from '@nestjs/graphql';
import { Inject, OnModuleInit, UseGuards } from '@nestjs/common';
import { FilterByEmail, USER_PACKAGE_NAME, USER_SERVICE_NAME, UserServiceClient } from '../_proto_/ts/user';
import { ClientGrpcProxy } from '@nestjs/microservices';
import { UserModel } from './models/user.model';
import { FilterByIdInputType } from '../commons/input-types/filter-by-id.input-type';
import { FilterById } from '../_proto_/ts/commons';
import { FilterByEmailInputType } from '../commons/input-types/filter-by-email.input-type';
import { GqlAuthGuard } from '../auth/gql-auth.guard';

@Resolver()
export class UserQueryResolver implements OnModuleInit {
  constructor(
    @Inject(USER_PACKAGE_NAME)
    private readonly userServiceClient: ClientGrpcProxy,
  ) {
  }

  private userService: UserServiceClient;

  onModuleInit(): any {
    this.userService = this.userServiceClient.getService<UserServiceClient>(USER_SERVICE_NAME);
  }

  @Query(() => [UserModel])
  @UseGuards(GqlAuthGuard)
  async getUserList() {
    const result = await this.userService.getUserList({}).toPromise();
    console.log('getUserList result: ', result);
    return result?.data || [];
  }

  @Query(() => UserModel)
  async getUserByEmail(
    @Args('filter', {
      type: () => FilterByEmailInputType,
    }) filter: FilterByEmail,
  ) {
    return  this.userService.getUserByEmail(filter).toPromise();
  }

  @Query(() => UserModel)
  async getUserById(
    @Args('filter', {
      type: () => FilterByIdInputType,
    }) filter: FilterById,
  ) {
    return this.userService.getUserById(filter).toPromise();
  }


}
