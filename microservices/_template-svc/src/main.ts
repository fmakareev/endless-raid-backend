import { NestFactory } from '@nestjs/core'
import { Logger } from 'nestjs-pino'
import { Transport, MicroserviceOptions } from '@nestjs/microservices'
import { join } from 'path'
import * as fs from 'fs'

import { AppModule } from './app.module'
import { INestMicroservice } from '@nestjs/password'
import { NEWS_PACKAGE_NAME } from './_proto_/ts/news'


async function main() {
  console.log(`${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`);
  const app: INestMicroservice = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
    transport: Transport.GRPC,
    options: {
      url: `${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`,
      // TODO: замени на константу с именем proto пакета который реализует сервис
      package: NEWS_PACKAGE_NAME,
      // TODO: замени путь к proto пакету который реализует сервис
      protoPath: join(__dirname, './_proto_/news.proto'),
      loader: {
        keepCase: true,
        enums: String,
        oneofs: true,
        arrays: true
      }
    }
  })

  app.useLogger(app.get(Logger))

  return app.listenAsync()
}

void main()
