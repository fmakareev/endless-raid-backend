import { PassportStrategy } from '@nestjs/passport'
import { ConfigService } from '@nestjs/config'
import { Injectable, OnModuleInit, Inject } from '@nestjs/common'
import { ClientGrpcProxy } from '@nestjs/microservices'

import { PinoLogger } from 'nestjs-pino'
import { Strategy, ExtractJwt } from 'passport-jwt'

import { USER_PACKAGE_NAME, USER_SERVICE_NAME, UserServiceClient, User } from '../_proto_/ts/user';
import { JwtTokenExtractor } from './jwt-token.extractor'

export const JwtStrategyName = 'jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, JwtStrategyName) implements OnModuleInit {
  constructor(
    @Inject(USER_PACKAGE_NAME)
    private readonly userServiceClient: ClientGrpcProxy,

    private readonly configService: ConfigService,

    private readonly logger: PinoLogger
  ) {
    super({
      secretOrKey: configService.get<string>('JWT_ACCESS_TOKEN_SECRET'),
      issuer: configService.get<string>('JWT_ISSUER'),
      audience: configService.get<string>('JWT_AUDIENCE'),
      jwtFromRequest: ExtractJwt.fromExtractors([
        JwtTokenExtractor.getTokenFromCookie('access-token'),
        JwtTokenExtractor.getTokenFromHeader('access-token'),
      ])
    })

    logger.setContext(JwtStrategy.name)
  }

  private userService: UserServiceClient;

  onModuleInit(): any {
    this.userService = this.userServiceClient.getService<UserServiceClient>(USER_SERVICE_NAME)
  }

  /** @desc получает данные из хэша токена и делает проверка на наличие пользователя в бд */
  async validate(payload: any): Promise<User> {
    return this.userService.getUserById({
      id: payload.user.id,
    }).toPromise()
  }
}
