import {Inject, OnModuleInit} from "@nestjs/common";
import {Args, Mutation, Resolver} from "@nestjs/graphql";
import {NEWS_PACKAGE_NAME, NEWS_SERVICE_NAME, NewsServiceClient, News} from "../_proto_/ts/news";
import {ClientGrpcProxy} from "@nestjs/microservices";
import {NewsModel} from "./models/news.model";
import {NewsCreateInputType} from "./input-types/news-create.input-type";
import {FilterByIdInputType} from "../commons/input-types/filter-by-id.input-type";
import {FilterById} from "../_proto_/ts/commons";
import { NewsUpdateInputType } from "./input-types/news-update.input-type";
import {MongooseDeleteResultObjectType} from "../commons/object-types/mongoose-delete-result.object-type";


@Resolver()
export class NewsMutationResolver implements  OnModuleInit{
    constructor(
        @Inject(NEWS_PACKAGE_NAME)
        private readonly newsServiceClient: ClientGrpcProxy
    ) {
    }

    private newsService: NewsServiceClient;

    onModuleInit(): any {
        this.newsService = this.newsServiceClient.getService<NewsServiceClient>(NEWS_SERVICE_NAME)
    }

    @Mutation(() => NewsModel)
    async createNews(
        @Args('params', {
            type: () => NewsCreateInputType
        }) params: News
    ) {
        const result = await this.newsService.createNews(params).toPromise();
        console.log('result: ', result);
        return result
    }

    @Mutation(() => NewsModel)
    async updateNews(
        @Args('params', {
            type: () => NewsUpdateInputType
        }) params: News
    ) {
        return this.newsService.updateNews(params).toPromise();
    }

    @Mutation(() => MongooseDeleteResultObjectType)
    async deleteNews(
        @Args('params', {
            type: () => FilterByIdInputType
        }) params: FilterById
    ) {
        return this.newsService.deleteNews(params).toPromise();
    }

}