import { Query, Resolver, Args } from '@nestjs/graphql';
import { NewsModel } from './models/news.model';
import { ClientGrpcProxy } from '@nestjs/microservices';
import { Inject, OnModuleInit } from '@nestjs/common';
import { NEWS_PACKAGE_NAME, NEWS_SERVICE_NAME, NewsServiceClient, NewsFilter } from '../_proto_/ts/news';
import { FilterById } from 'src/_proto_/ts/commons';
import { FilterByIdInputType } from '../commons/input-types/filter-by-id.input-type';
import { NewsFilterInput } from './input-types/news-filter.input-type';


@Resolver(() => NewsModel)
export class NewsQueryResolver implements OnModuleInit {


  constructor(
    @Inject(NEWS_PACKAGE_NAME)
    private readonly newsServiceClient: ClientGrpcProxy,
  ) {
  }

  private newsService: NewsServiceClient;

  onModuleInit(): any {
    this.newsService = this.newsServiceClient.getService<NewsServiceClient>(NEWS_SERVICE_NAME);
  }

  @Query(() => [NewsModel])
  async getNewsForAdmin() {
    const result = await this.newsService.getNewsForAdmin({}).toPromise();

    return result.data;
  }

  @Query(() => [NewsModel])
  async getNewsForClient(
    @Args('filter', { type: () => NewsFilterInput }) filter: NewsFilter,
  ) {
    const result = await this.newsService.getNewsForClient(filter).toPromise();
    return result?.data || [];
  }

  @Query(() => NewsModel)
  getNewsById(
    @Args('filter', {
      type: () => FilterByIdInputType,
    }) filter: FilterById,
  ) {
    return this.newsService.getNewsById(filter).toPromise();
  }


}
