import {Controller, Inject } from '@nestjs/common';
import {GrpcMethod} from '@nestjs/microservices';
import {Empty, FilterById, MongooseDeleteResult} from "../_proto_/ts/commons";
import {News, NEWS_SERVICE_NAME, NewsServiceController, NewsFilter} from "../_proto_/ts/news";

@Controller()
export class NewsController implements NewsServiceController {
    constructor(
        @Inject(NEWS_SERVICE_NAME)
        private readonly service: NewsServiceController,
    ) {
    }

    @GrpcMethod(NEWS_SERVICE_NAME, 'GetNewsForClient')
    getNewsForClient(request: NewsFilter) {
        return this.service.getNewsForClient(request, new Date().toISOString());
    }

    @GrpcMethod(NEWS_SERVICE_NAME, 'GetNewsForAdmin')
    getNewsForAdmin(req: Empty) {
        return this.service.getNewsForAdmin(req);
    }

    @GrpcMethod(NEWS_SERVICE_NAME, 'GetNewsById')
    getNewsById(props: FilterById) {
        return this.service.getNewsById(props);
    }

    @GrpcMethod(NEWS_SERVICE_NAME, 'DeleteNews')
    async deleteNews(request: FilterById): Promise<MongooseDeleteResult> {
       return this.service.deleteNews(request) as MongooseDeleteResult;
    }

    @GrpcMethod(NEWS_SERVICE_NAME, 'CreateNews')
    createNews(props: News) {
        return this.service.createNews(props);
    }

    @GrpcMethod(NEWS_SERVICE_NAME, 'UpdateNews')
    updateNews(props: News) {
        return this.service.updateNews(props);
    }

}
