import {Test, TestingModule} from '@nestjs/testing';
import {NewsService} from './news.service';
import {MongoMemoryServer} from 'mongodb-memory-server';
import * as mongoose from "mongoose";
import {MongooseModule} from '@nestjs/mongoose';
import {NewsSchema} from "./schemas/news.schema";
import {News} from 'src/_proto_/ts/news';

describe('NewsService', () => {
    let service: NewsService;
    let mongod;
    let module: TestingModule;
    afterEach(async () => {
        await module.close();
        await mongoose.disconnect();
        await mongod.stop();
    });

    beforeEach(async () => {
        mongod = new MongoMemoryServer();
        module = await Test.createTestingModule({
            imports: [
                MongooseModule.forRootAsync({
                    useFactory: async () => ({
                        uri: await mongod.getUri(),
                    }),
                }),
                MongooseModule.forFeature([
                    {
                        name: 'News',
                        schema: NewsSchema
                    }
                ])
            ],
            providers: [NewsService],
        }).compile();
        service = module.get<NewsService>(NewsService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('createNews', async () => {
        const news = [
            {
                isActive: true,
                title: '',
                publicationStartDate: new Date().toISOString(),
                publicationEndDate: new Date().toISOString(),
            },
            {
                isActive: true,
                title: '',
                publicationStartDate: new Date().toISOString(),
                publicationEndDate: new Date().toISOString(),
            },
        ]
        await service.createNews(news[0])
        await service.createNews(news[1])
        const newsFromDB = await service.getNewsForAdmin()
        console.log(newsFromDB.data);
        expect(newsFromDB.data).toHaveLength(2);
    })

    it('getNewsForClient', async () => {
        const fromPublishDate = "2020-10-15T11:00:00.000Z";
        const currentDate: any = "2020-10-15T13:00:00.000Z";


        /** дата начала >= дате последнего обновления && дата окончания >= текущей дате && активна */
        const newsCase1 = {
            isActive: true,
            title: '1',
            publicationStartDate: "2020-10-15T12:00:00.000Z",
            publicationEndDate: "2020-10-15T15:00:00.000Z",
        }
        /** дата начала >= дате последнего обновления && дата окончания >= текущей дате && активна */
        const newsCase2 = {
            isActive: false,
            title: '2',
            publicationStartDate: "2020-10-15T12:00:00.000Z",
            publicationEndDate: "2020-10-15T15:00:00.000Z",
        };
        /** дата начала < дате последнего обновления && дата окончания >= текущей дате && активна */
        const newsCase3 = {
            isActive: true,
            title: '3',
            publicationStartDate: "2020-10-15T10:00:00.000Z",
            publicationEndDate: "2020-10-15T15:00:00.000Z",
        }

        await service.createNews(newsCase1)
        await service.createNews(newsCase2)
        await service.createNews(newsCase3)

        const newsFromDB = await service.getNewsForClient({fromPublishDate}, currentDate)

        expect( newsFromDB.data).toHaveLength(1);
        expect( newsFromDB.data[0]).toMatchObject({
            title: '1',
            isActive: true,
        })

    })
});
