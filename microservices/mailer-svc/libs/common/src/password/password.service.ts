import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';


@Injectable()
export class PasswordService {

    async compare(password: string, hash: string): Promise<boolean> {
        return bcrypt.compare(password, hash)
    }

    async hash(password): Promise<string> {
        const saltRounds = 10;
        return bcrypt.hash(password, saltRounds);
    }

}
