import {Module} from '@nestjs/common';
import {NewsQueryResolver} from './news-query.resolver';
import {ClientGrpcProxy, ClientProxyFactory, Transport} from "@nestjs/microservices";
import {NEWS_PACKAGE_NAME, protobufPackage} from "../_proto_/ts/news";
import {join} from "path";
import {ConfigService, ConfigModule} from "@nestjs/config";
import {LoggerModule} from 'nestjs-pino';
import { NewsMutationResolver } from './news-mutation.resolver';

@Module({
    imports: [
        ConfigModule,
        LoggerModule,
    ],
    providers: [
        {
            provide: NEWS_PACKAGE_NAME,
            useFactory: (configService: ConfigService): ClientGrpcProxy => {
                console.log(configService.get<string>('NEWS_SVC_URL'));

                return ClientProxyFactory.create({
                    transport: Transport.GRPC,
                    options: {
                        url: configService.get<string>('NEWS_SVC_URL'),
                        package: protobufPackage,
                        protoPath: join(__dirname, '/_proto_/news.proto'),
                    }
                })
            },
            inject: [ConfigService],
        },
        NewsQueryResolver,
        NewsMutationResolver
    ]
})
export class NewsModule {
}
