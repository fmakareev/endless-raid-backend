import { Schema, type } from "@colyseus/schema";
import { UserSchema } from "./user.schema";

export class PlayerSchema extends Schema {
  @type(UserSchema)
  user: UserSchema;

  @type('boolean')
  isHost: boolean;

  @type('string')
  sessionId: string;
}
