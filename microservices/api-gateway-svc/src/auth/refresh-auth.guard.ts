import { Injectable, ExecutionContext } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { GqlExecutionContext } from '@nestjs/graphql'
import { JwtRefreshStrategyName } from './jwt-refresh.strategy';

@Injectable()
export class RefreshAuthGuard extends AuthGuard(JwtRefreshStrategyName) {
  getRequest(context: ExecutionContext): any {
    const ctx = GqlExecutionContext.create(context)
    return ctx.getContext().req
  }
}
