/* eslint-disable */
import { Id } from './commons';
import { Observable } from 'rxjs';
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';


export interface Reward {
  id: string;
  title: string;
  description: string;
}

export interface RewardServiceClient {

  getRewardById(request: Id, ...rest: any): Observable<Reward>;

}

export interface RewardServiceController {

  getRewardById(request: Id, ...rest: any): Promise<Reward> | Observable<Reward> | Reward;

}

export function RewardServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getRewardById'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod('RewardService', method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod('RewardService', method)(constructor.prototype[method], method, descriptor);
    }
  }
}

export const protobufPackage = 'reward'

export const REWARD_PACKAGE_NAME = 'reward'
export const REWARD_SERVICE_NAME = 'RewardService';