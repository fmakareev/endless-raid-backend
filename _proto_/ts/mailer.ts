/* eslint-disable */
import { Observable } from 'rxjs';
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';


export interface SendMailInput {
  template: MailTemplate;
  to: string;
  data: Uint8Array;
}

export interface SendMailPayload {
  isSent: boolean;
}

export interface MailerServiceClient {

  send(request: SendMailInput, ...rest: any): Observable<SendMailPayload>;

}

export interface MailerServiceController {

  send(request: SendMailInput, ...rest: any): Promise<SendMailPayload> | Observable<SendMailPayload> | SendMailPayload;

}

export function MailerServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['send'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod('MailerService', method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod('MailerService', method)(constructor.prototype[method], method, descriptor);
    }
  }
}

export const protobufPackage = 'mailer'

export enum MailTemplate {
  SIGNUP = 0,
  UPDATE_EMAIL = 1,
  UPDATE_PASSWORD = 2,
  RECOVERY_PASSWORD = 3,
  UNRECOGNIZED = -1,
}

export const MAILER_PACKAGE_NAME = 'mailer'
export const MAILER_SERVICE_NAME = 'MailerService';