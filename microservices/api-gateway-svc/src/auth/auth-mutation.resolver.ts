import { Inject, OnModuleInit, UnauthorizedException, UseGuards } from '@nestjs/common';
import { Mutation, Resolver, Args, Context } from '@nestjs/graphql';
import { LoginInputType } from './input-types/login.input-type';
import { UserModel } from '../user/models/user.model';
import { ClientGrpcProxy } from '@nestjs/microservices';
import { UserServiceClient, USER_SERVICE_NAME, USER_PACKAGE_NAME, User } from 'src/_proto_/ts/user';
import { PasswordService } from '@app/common/password/password.service';
import { AuthService } from './auth.service';
import { CurrentUser } from './user.decorator';
import { RefreshAuthGuard } from './refresh-auth.guard';


@Resolver()
export class AuthMutationResolver implements OnModuleInit {

  constructor(
    @Inject(USER_PACKAGE_NAME)
    private readonly userServiceClient: ClientGrpcProxy,
    private readonly passwordService: PasswordService,
    private readonly authService: AuthService,
  ) {
  }

  private userService: UserServiceClient;

  onModuleInit(): any {
    this.userService = this.userServiceClient.getService<UserServiceClient>(USER_SERVICE_NAME);
  }

  @Mutation(() => UserModel)
  async login(
    @Args('params', {
      type: () => LoginInputType,
    }) params,
    @Context() context: any,
  ) {
    const { res } = context;
    const user = await this.userService.getUserByEmail({
      email: params.email,
    }).toPromise();

    const isValidPassword: boolean = await this.passwordService.compare(params.password, user.password);

    if (!isValidPassword) {
      throw new UnauthorizedException();
    }

    await this.authService.setTokens({
      res,
      user,
    });
    return user;
  }


  @Mutation(() => UserModel)
  @UseGuards(RefreshAuthGuard)
  async refreshToken(@Context() context: any, @CurrentUser() user: User): Promise<User> {
    const { res } = context;
    console.log('refreshToken: ', user);
    await this.authService.setTokens({
      res,
      user,
    });

    return user;
  }


}
