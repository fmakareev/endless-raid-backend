import {Field, InputType, GraphQLISODateTime} from "@nestjs/graphql";

@InputType()
export class NewsCreateInputType {
    @Field()
    title: string;
    @Field()
    description: string;
    @Field()
    buttonText: string;
    @Field()
    buttonUrl: string;
    @Field()
    smallPreview: string;
    @Field()
    largePreview: string;
    @Field( () => GraphQLISODateTime)
    publicationStartDate: string;
    @Field(() => GraphQLISODateTime)
    publicationEndDate: string;
    @Field()
    shortDesc: string;
    @Field({
        defaultValue: false
    })
    isActive: boolean;
}
