import * as Joi from 'joi';

export const CreateUserValidation = Joi.object({
    email: Joi.string().email(),
    password: Joi.string().min(8).optional(),
})