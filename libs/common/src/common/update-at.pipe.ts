import {Injectable, PipeTransform} from '@nestjs/common';
import {Metadata} from "grpc";


@Injectable()
export class UpdateAtPipe implements PipeTransform {
    transform(value: any) {
        if (value instanceof Metadata) {
            return value;
        }
        return {
            ...value,
            updateAt: new Date().toISOString(),
        };
    }
}
