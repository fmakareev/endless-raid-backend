import { ConfigModule } from '@nestjs/config';

const environment = process.env.NODE_ENV || 'development';
console.log('environment: ', environment);
export const configModule = ConfigModule.forRoot({
    envFilePath: `.env.${environment}`,
    isGlobal: true,
})