import { NestFactory } from '@nestjs/core'
import { Logger } from 'nestjs-pino'
import { Transport, MicroserviceOptions } from '@nestjs/microservices'
import { join } from 'path'

import { AppModule } from './app.module'
import { MAILER_PACKAGE_NAME } from './_proto_/ts/mailer'
import { INestMicroservice } from '@nestjs/common'


async function main() {
  console.log(`${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`);
  const app: INestMicroservice = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
    transport: Transport.GRPC,
    options: {
      url: `${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`,
      package: MAILER_PACKAGE_NAME,
      protoPath: join(__dirname, './_proto_/mailer.proto'),
      loader: {
        keepCase: true,
        enums: String,
        oneofs: true,
        arrays: true
      }
    }
  })

  app.useLogger(app.get(Logger))

  return app.listenAsync()
}

void main()
