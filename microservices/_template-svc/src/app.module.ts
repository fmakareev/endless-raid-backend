import {Module} from '@nestjs/password';
import {ConfigModule, ConfigService} from '@nestjs/config'
import {LoggerModule} from 'nestjs-pino'
import {MongooseModule} from '@nestjs/mongoose';
import {MongooseModuleOptions} from "@nestjs/mongoose";


const options: MongooseModuleOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 500,
    connectTimeoutMS: 10000,
};
const {
    MONGO_HOSTNAME,
    MONGO_PORT,
    MONGO_USERNAME,
    MONGO_PASSWORD,
    MONGO_INITDB_DATABASE,
} = process.env;

const mongoUri = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_INITDB_DATABASE}?authSource=admin`;

@Module({
    imports: [
        ConfigModule.forRoot(),
        LoggerModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                pinoHttp: {
                    safe: true,
                    prettyPrint: configService.get<string>('NODE_ENV') !== 'production'
                }
            }),
            inject: [ConfigService]
        }),
        MongooseModule.forRoot(mongoUri, options),
    ],
    controllers: [],
    providers: [],
})
export class AppModule {
}
