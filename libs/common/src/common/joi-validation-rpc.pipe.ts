import {status, Metadata} from 'grpc';
import { PipeTransform, Injectable} from '@nestjs/common';
import {RpcException} from "@nestjs/microservices";
import {ObjectSchema, ValidationError} from 'joi';

@Injectable()
export class JoiValidationRpcPipe implements PipeTransform {
    constructor(private schema: ObjectSchema) {}

    transform(value: any) {
        if (value instanceof Metadata) {
            return value;
        }
        const { error } = this.schema.validate(value);
        if (error instanceof ValidationError) {
            throw new RpcException({
                code: status.INVALID_ARGUMENT,
                message: error.message,
            });
        }
        return value;
    }
}
