import { Module } from '@nestjs/common';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { LoggerModule } from 'nestjs-pino'
import {MongooseModule} from "@nestjs/mongoose";
import {NewsSchema} from "./schemas/news.schema";

@Module({
  imports: [
      LoggerModule,
      MongooseModule.forFeature([
        {
          name: 'News',
          schema: NewsSchema
        }
      ])
  ],
  controllers: [NewsController],
  providers: [NewsService]
})
export class NewsModule {}
