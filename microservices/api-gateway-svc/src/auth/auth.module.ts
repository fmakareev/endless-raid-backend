import { Module, forwardRef } from '@nestjs/common';
import { AuthMutationResolver } from './auth-mutation.resolver';
import { AuthService } from './auth.service';
import { UserModule } from 'src/user/user.module';
import { PasswordService, PasswordModule } from '@app/common';
import { JwtAccessTokenService, JwtRefreshTokenService } from './jwt-token.service';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { JwtRefreshStrategy } from './jwt-refresh.strategy';

@Module({
  imports: [
    PasswordService,
    PasswordModule,
    PassportModule.register({
      defaultStrategy: 'jwt',
    }),
    forwardRef(() => UserModule),
  ],
  providers: [
    AuthService,
    JwtStrategy,
    JwtRefreshStrategy,
    AuthMutationResolver,
    JwtAccessTokenService,
    JwtRefreshTokenService,
  ],
  exports: [AuthService, JwtAccessTokenService, JwtRefreshTokenService],
})
export class AuthModule {
}
