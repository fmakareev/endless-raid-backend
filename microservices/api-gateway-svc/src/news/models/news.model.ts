import {Field, GraphQLISODateTime, ObjectType} from '@nestjs/graphql';

@ObjectType()
export class NewsModel {

    @Field({nullable: false})
    id: string;

    @Field()
    title: string;

    @Field()
    description: string;
    @Field()
    buttonText: string;
    @Field()
    smallPreview: string;
    @Field()
    largePreview: string;
    @Field()
    shortDesc: string;
    @Field()
    isActive: boolean;
    @Field(() => GraphQLISODateTime)
    publicationStartDate: string;
    @Field(() => GraphQLISODateTime)
    publicationEndDate: string;
}