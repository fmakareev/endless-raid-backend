import { Args, Context, Mutation, Resolver } from '@nestjs/graphql';
import { forwardRef, Inject, OnModuleInit, UnauthorizedException, UseGuards } from '@nestjs/common';
import { ClientGrpcProxy } from '@nestjs/microservices';
import {
  User,
  USER_PACKAGE_NAME,
  USER_SERVICE_NAME,
  UserServiceClient,
  CreateUserDto,
  UserPasswordDto,
  ChangeUserEmailDto,
  UserGuestUpdateDto,
} from '../_proto_/ts/user';
import { UserModel } from './models/user.model';
import { AuthService } from '../auth/auth.service';
import { CreateUserInputType } from './input-types/create-user.input-type';
import { UpdateUserPasswordInputType } from './input-types/update-user-password.input-type';
import { PasswordService } from '@app/common';
import { GqlAuthGuard } from '../auth/gql-auth.guard';
import {
  isEmpty,
} from 'lodash';
import { UpdateUserEmailInputType } from './input-types/update-user-email.input-type';
import { GraphQLError } from 'graphql';
import { UpdateGuestUserInputType } from './input-types/update-guest-user.input-type';


@Resolver()
export class UserMutationResolver implements OnModuleInit {
  constructor(
    @Inject(USER_PACKAGE_NAME)
    private readonly userServiceClient: ClientGrpcProxy,
    @Inject(forwardRef(() => AuthService))
    private readonly authService: AuthService,
    private readonly passwordService: PasswordService,
  ) {
  }

  private userService: UserServiceClient;

  onModuleInit(): any {
    this.userService = this.userServiceClient.getService<UserServiceClient>(USER_SERVICE_NAME);
  }

  @Mutation(() => UserModel)
  async createUser(
    @Context() context: any,
    @Args('params', {
      type: () => CreateUserInputType,
    }) params: CreateUserDto,
  ) {
    const { res } = context;
    const user = await this.userService.createUser(params).toPromise();

    await this.authService.setTokens({
      res,
      user,
    });

    return user;
  }

  @Mutation(() => UserModel)
  async createGuestUser(@Context() context: any) {
    const { res } = context;
    const user: User = await this.userService.createGuestUser({}).toPromise();

    await this.authService.setTokens({
      res,
      user,
    });

    return user;
  }

  @Mutation(() => UserModel)
  @UseGuards(GqlAuthGuard)
  async changePassword(
    @Context() context: any,
    @Args('params', {
      type: () => UpdateUserPasswordInputType,
    }) params: UserPasswordDto,
  ) {

    const { res } = context;
    const user = await this.userService.getUserByEmail({
      email: params.email,
    }).toPromise();

    if (isEmpty(user)) {
      throw new GraphQLError('Password incorrect.');
    }

    const isValidPassword: boolean = await this.passwordService.compare(params.oldPassword, user.password);
    if (!isValidPassword) {
      throw new UnauthorizedException();
    }

    await this.userService.changePassword(params).toPromise();
    await this.authService.setTokens({
      res,
      user,
    });

    return user;
  }

  @Mutation(() => UserModel)
  @UseGuards(GqlAuthGuard)
  async changeEmail(
    @Context() context: any,
    @Args('params', {
      type: () => UpdateUserEmailInputType,
    }) params: ChangeUserEmailDto,
  ) {
    const { res } = context;
    const user = await this.userService.changeEmail(params).toPromise();

    await this.authService.setTokens({
      res,
      user,
    });

    return user;
  }

  @Mutation(() => UserModel, {
    description: 'Updating guest profile to player using email and password'
  })
  @UseGuards(GqlAuthGuard)
  async updateGuestUser(
    @Context() context: any,
    @Args('params', {
      type: () => UpdateGuestUserInputType,
    }) params: UserGuestUpdateDto,
  ) {
    const { res } = context;
    const user = await this.userService.updateGuestUser(params).toPromise();
    await this.authService.setTokens({
      res,
      user,
    });

    return user;
  }

}
