import {Field, InputType} from "@nestjs/graphql";

@InputType()
export class FilterByIdInputType {
    @Field()
    id: string;
}