import { Room, Client, generateId } from "colyseus";
import { Schema, MapSchema, ArraySchema, Context } from "@colyseus/schema";
import { verifyToken, User, IUser } from "@colyseus/social";

// Create a context for this room's state data.
const type = Context.create();

class Entity extends Schema {
  @type("number") x: number = 0;
  @type("number") y: number = 0;
}

class Player extends Entity {
  @type("boolean") connected: boolean = true;
}

class Enemy extends Entity {
  @type("number") power: number = Math.random() * 10;
}

class State extends Schema {
  @type({ map: Entity }) entities = new MapSchema<Entity>();
}

/**
 * Demonstrate sending schema data types as messages
 */
class Message extends Schema {
  @type("number") num;
  @type("string") str;
}

export class DemoRoom extends Room {

  onCreate (options: any) {
    console.log("DemoRoom created.", options);

    this.setState(new State());
    this.populateEnemies();

    this.setMetadata({
      str: "hello",
      number: 10
    });

    this.setPatchRate(1000 / 20);

    this.onMessage(0, (client, message) => {
      client.send(0, message);
    });

    this.onMessage("schema", (client) => {
      const message = new Message();
      message.num = Math.floor(Math.random() * 100);
      message.str = "sending to a single client";
      client.send(message);
    })

    this.onMessage("move_right", (client) => {
      this.state.entities[client.sessionId].x += 0.01;

      this.broadcast("hello", { hello: "hello world" });
    });

    this.onMessage("*", (client, type, message) => {
      console.log(`received message "${type}" from ${client.sessionId}:`, message);
    });
  }
  populateEnemies () {
    for (let i=0; i<=3; i++) {
      const enemy = new Enemy();
      enemy.x = Math.random() * 2;
      enemy.y = Math.random() * 2;
      this.state.entities[generateId()] = enemy;
    }
  }

  onJoin (client: Client, options: any, user: IUser) {
    console.log("client joined!", client.sessionId);
    this.state.entities[client.sessionId] = new Player();

    client.send("type", { hello: true });
  }

}
