import {Field, ObjectType} from '@nestjs/graphql';


@ObjectType()
export class MongooseDeleteResultObjectType {
    @Field({
        description: 'number of matched documents'
    })
    n: number
    @Field({
        description: '1 if the operation was successful'
    })
    ok: number;
    @Field({
        description: 'number of documents deletedCount'
    })
    deletedCount: number;
}