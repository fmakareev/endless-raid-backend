import * as mongoose from 'mongoose';
import { UserRole } from '../../_proto_/ts/user';


export const USER_MODEL_NAME = 'User';
export const UserSchema = new mongoose.Schema({
  userName: {
    type: String,
  },
  email: { type: String },

  role: {
    type: Number,
    default: UserRole.GUEST,
    required: true,
  },
  password: {
    type: String,
  },
  passwordSalt: {
    type: String,
  },
  createAt: {
    type: Date,
  },
  updateAt: {
    type: Date,
  },
});

UserSchema.index('email',{
  unique: true,
  partialFilterExpression: {
    'email': { $exists: true, $gt: '' }
  }
})