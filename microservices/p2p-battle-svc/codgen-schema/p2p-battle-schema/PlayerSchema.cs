// 
// THIS FILE HAS BEEN GENERATED AUTOMATICALLY
// DO NOT CHANGE IT MANUALLY UNLESS YOU KNOW WHAT YOU'RE DOING
// 
// GENERATED USING @colyseus/schema 1.0.1
// 

using Colyseus.Schema;

namespace P2PBattleStateSchemaNS {
	public class PlayerSchema : Schema {
		[Type(0, "ref", typeof(UserSchema))]
		public UserSchema user = new UserSchema();

		[Type(1, "boolean")]
		public bool isHost = default(bool);

		[Type(2, "string")]
		public string sessionId = default(string);
	}
}
