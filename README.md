
# Project structure

* _proto_ - proto buffers схемы и ts типы
* microservices
* libs - общий для всех библиотек код, тулзы и т.д.
* Makefile - утилиты для работы с контейнерами и кодогенерацией 

# Зависимости

* [docker](https://docs.docker.com/engine/install/ubuntu/)
* [docker-compose](https://docs.docker.com/compose/install/)
* [nestjs](https://docs.nestjs.com/)
* [make](https://askubuntu.com/questions/161104/how-do-i-install-make)
* [node.js](https://nodejs.org/en/)
* [protobuf](https://snapcraft.io/protobuf)
* [BloomRPC](https://github.com/uw-labs/bloomrpc) - postman для отладки grpc

# Полезное

* [code style guide](https://angular.io/guide/styleguide)
* [grpc](https://grpc.io/)
* [nestjs](https://docs.nestjs.com/)

# Сервисы

* [Документация по сервисам](https://www.notion.so/f7d1e9f423c8497ea83e07f3022891ed?v=613a2fcdb7c740d5892ce8b9ea444ea0)
* [Модели](https://www.notion.so/0c732509d956474eb3c75790440f22e2)


# Create new service

> В будущем упростим развертывание через yeoman, когда утвердим шаблон сервиса   

1. В дирректории microservices есть шаблон микросервиса _template-svc, 
клонируем его и заменяем через авторефакторинг (Webstorm - Ctrl+Shift+R) имя <svc-name> на имя нового микросервиса.
2. npm install
3. npm run start:docker

# Особенности разработки

1. proto buffers хранятся в корне проекта в папке _proto_, после каждого изменения следует, 
пере генерировать typescript типы командой make proto-to-ts и перезапустить активные в разработке сервисы, 
для того чтобы обновить типы и proto файлы в исходниках сервиса
2. По умолчанию шаблон сервиса уже содержим mongodb, если она не требуется, то следует убрать ее из docker-compose файлов,
чтобы ускорить запуск контейнера
3. Все контейнеры запускаются в единой docker network, для режима dev и prod имея сети разные, 
для dev среды - er-network-dev, для prod среды - er-network. Чтобы проверить есть ли эти сети на хост машине, выполните 
docker network ls, пример вывода:

    ```shell script
       NETWORK ID          NAME                DRIVER              SCOPE
       4d6baca08f18        bridge              bridge              local
       c092cd11e94b        er-network          bridge              local
       ffd161c38f9d        er-network-dev      bridge              local
       5078bf70c4c3        host                host                local
       8c0d0059609b        none                null                local
    ```  
   подробнее про docker network [тут](https://docs.docker.com/network/)
4. Для того чтобы получить доступ по сети из одного сервиса в другой они должны быть в одной сети, 
а доступ можно получить по имени сервиса + порт на котором крутится приложение внутри контейнера (пример: news-svc:50051). 
5. Переменные среды задаем через файлы .env.{NODE_ENV}, в сервис api-gateway-svc при создании нового сервиса добавляем 
новую переменную среды со ссылкой на сервис. 
6. После добавления нового пакета в сервис стоит его перезапустить.

# Структура сервиса

В основном все стандартно как в nestjs, но добавлено пару скриптов:

* copy:libs - копирование библиотеки в сервис, (временное решение)
* copy:proto - копирование proto и ts типов в директорию сервиса
* copy - объединяет 2 предыдущих скрипта
* docker:build:dev - сборка контейнера для разработки
* start:docker - запуск проекта в docker контейнере в режиме для разработки

# Pipeline CI/CD

Авто deploy отключен, для доставки обновления сервиса на нужный стенд необходимо активировать pipeline вручную 
на странице Pipelines в bitbucket. Для каждого сервиса есть свой пайплайн и так же есть пайплайн 
для деплоя сразу всех сервисов.


# TODO:

1. Сделать деплой на стенд относительно ветки, из develop на dev стенд, из master на production стенд
2. Добавить автоматическое версионирование для контейнеров сервисов
3. Добавить пайплайн для отката до предыдущей версии любого из сервисов 