import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema, USER_MODEL_NAME } from './schemas/user.schema';
import { PasswordModule } from '@app/common';

@Module({
  imports: [
    PasswordModule,
    MongooseModule.forFeature([
      {
        name: USER_MODEL_NAME,
        schema: UserSchema
      }
    ])
  ],
  providers: [UserService],
  controllers: [UserController]
})
export class UserModule {}
