import {status} from 'grpc';
import {Error} from 'mongoose';
import {RpcException} from "@nestjs/microservices";

export const MongoErrorMatchRpcError = (error) => {

    let rpcException = {
        message: error.message,
        code: status.UNKNOWN,
    };

    if (error instanceof Error.CastError) {
        rpcException = {
            message: error.message,
            code: status.INVALID_ARGUMENT,
        }
    }

    if (error instanceof Error.ValidationError) {
        rpcException = {
            message: error.message,
            code: status.INVALID_ARGUMENT,
        }
    }


    throw new RpcException(rpcException);
}