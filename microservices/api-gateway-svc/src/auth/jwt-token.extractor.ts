import {Request} from 'express';
import {get} from 'lodash';


export class JwtTokenExtractor {

  static getTokenFromHeader = (tokenKey: string) => (req: Request) => req.header(tokenKey)
  static getTokenFromCookie = (tokenKey: string) => (req: Request) => get(req, `cookies.${tokenKey}`)

}