export enum ActionTypeEnum {
  Action = 'Action',
  ActionResult = 'ActionResult',
  StartGame = 'StartGame',
  EndGame = 'EndGame',
}
