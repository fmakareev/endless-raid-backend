import { PassportStrategy } from '@nestjs/passport'
import { ConfigService } from '@nestjs/config'
import { Injectable, OnModuleInit, Inject } from '@nestjs/common'
import { ClientGrpcProxy } from '@nestjs/microservices'

import { PinoLogger } from 'nestjs-pino'
import { Strategy, ExtractJwt } from 'passport-jwt'
import { USER_PACKAGE_NAME, USER_SERVICE_NAME, UserServiceClient, User } from 'src/_proto_/ts/user';
import { JwtTokenExtractor } from './jwt-token.extractor';

export const JwtRefreshStrategyName ='jwt-refresh'

@Injectable()
export class JwtRefreshStrategy extends PassportStrategy(Strategy, JwtRefreshStrategyName) implements OnModuleInit {
  constructor(
    @Inject(USER_PACKAGE_NAME)
    private readonly userServiceClient: ClientGrpcProxy,

    private readonly configService: ConfigService,

    private readonly logger: PinoLogger
  ) {
    super({
      secretOrKey: configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
      issuer: configService.get<string>('JWT_ISSUER'),
      audience: configService.get<string>('JWT_AUDIENCE'),
      jwtFromRequest: ExtractJwt.fromExtractors([
        JwtTokenExtractor.getTokenFromCookie('refresh-token'),
        JwtTokenExtractor.getTokenFromHeader('refresh-token'),
      ]),
    })

    logger.setContext(JwtRefreshStrategy.name)
  }

  private userService: UserServiceClient;

  onModuleInit(): void {
    this.userService = this.userServiceClient.getService<UserServiceClient>(USER_SERVICE_NAME)
  }

  async validate(payload: any): Promise<User> {
    return this.userService.getUserById({
      id: payload.user.id,
    }).toPromise()
  }
}
