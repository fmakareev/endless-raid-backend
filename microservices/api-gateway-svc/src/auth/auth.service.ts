import { Injectable, Inject } from '@nestjs/common';
import { User } from 'src/_proto_/ts/user';
import { JwtService } from '@nestjs/jwt';
import { PinoLogger } from 'nestjs-pino';
import { JwtAccessTokenServiceName } from './jwt-token.service';
import { JwtRefreshTokenServiceName } from './jwt-token.service';

@Injectable()
export class AuthService {
  constructor(
    @Inject(JwtAccessTokenServiceName)
    private readonly accessTokenService: JwtService,

    @Inject(JwtRefreshTokenServiceName)
    private readonly refreshTokenService: JwtService,

    private readonly logger: PinoLogger
  ) {
    logger.setContext(AuthService.name)
  }

  async generateAccessToken(user: User): Promise<string> {
    return this.accessTokenService.sign(
      {
        user: {
          id: user.id,
          role: user.role,
        }
      },
      {
        subject: user.id
      }
    )
  }

  async generateRefreshToken(user: User): Promise<string> {
    return this.refreshTokenService.sign(
      {
        user: {
          id: user.id,
          email: user.email,
        }
      },
      {
        subject: user.id
      }
    )
  }

  public async setTokens({ res, user }) {
    const accessToken = await this.generateAccessToken(user);
    const refreshToken = await this.generateRefreshToken(user);
    res.setHeader('access-token', accessToken);
    res.setHeader('refresh-token', refreshToken);
    res.cookie('access-token',accessToken, {
      httpOnly: true,
      maxAge: 1.8e6,
    });
    res.cookie('refresh-token', refreshToken, {
      httpOnly: true,
      maxAge: 1.728e8,
    });
  }
}
