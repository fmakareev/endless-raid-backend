import {Field, InputType} from "@nestjs/graphql";

@InputType()
export class NewsUpdateInputType {
    @Field()
    id: string;
    @Field({ nullable: true })
    title: string;
    @Field({ nullable: true })
    description: string;
    @Field({ nullable: true })
    buttonText: string;
    @Field({ nullable: true })
    buttonUrl: string;
    @Field({ nullable: true })
    smallPreview: string;
    @Field({ nullable: true })
    largePreview: string;
    @Field({ nullable: true })
    publicationStartDate: string;
    @Field({ nullable: true })
    publicationEndDate: string;
    @Field({ nullable: true })
    shortDesc: string;
    @Field({ nullable: true })
    isActive: boolean;
}
