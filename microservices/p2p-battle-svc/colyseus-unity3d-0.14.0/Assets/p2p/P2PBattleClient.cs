using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Colyseus;
using Colyseus.Schema;
using System;
using P2PBattleStateSchemaNS;
using GameDevWare.Serialization;




public class P2PBattleClient : MonoBehaviour
{

	protected Client client;
	protected Room<P2PBattleRoomState> room;


	void Start()
    {
		ConnectToServer();
	}

	public void ConnectToServer()
	{
		UnityEngine.Debug.Log("some text");


		//string endpoint = "ws://192.168.0.177/v1/p2p";
		string endpoint = "ws://195.2.81.252/v1/p2p";

		Debug.Log("Connecting to " + endpoint);

		client = new Client(endpoint);
		CreateRoom();
	}


	public async void CreateRoom()
	{

		Dictionary<string, string> user = new Dictionary<string, string>
			{
				{ "email", " asd"},
				{ "id", "asd"},
				{ "userName", "asd"},
				{ "role", "asd"}
			};


		Dictionary<string, object> options = new Dictionary<string, object>
		{
			{"user", user }
		};
		UnityEngine.Debug.Log("CreateRoom");
		room = await client.Create<P2PBattleRoomState>("P2PBattleRoom", options);

		room.OnStateChange += (state, isFirstState) =>
		{
			if (isFirstState)
			{
				Debug.Log("this is the first room state!");
			}

			Debug.Log("the room state has been updated");
		};

		UnityEngine.Debug.Log("sessionId: " + room.SessionId);
		UnityEngine.Debug.Log("Id: " + room.Id);
	}

}
