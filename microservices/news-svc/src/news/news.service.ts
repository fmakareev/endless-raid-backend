import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model, Document} from 'mongoose';
import {News, NewsList, NewsServiceController, NewsFilter} from '../_proto_/ts/news';
import {FilterById, MongooseDeleteResult} from "../_proto_/ts/commons";
import {MongoErrorMatchRpcError} from '@app/common';

@Injectable()
export class NewsService implements NewsServiceController {

    constructor(@InjectModel('News') private readonly newsModel: Model<News & Document>) {
    }

    createNews(props): Promise<News> {
        const news = new this.newsModel(props);
        return news.save().catch(MongoErrorMatchRpcError)
    }

    async deleteNews(request: FilterById): Promise<MongooseDeleteResult> {
        const result: any = await this.newsModel.deleteOne({_id: request.id}).exec()
            .catch(MongoErrorMatchRpcError);
        return result as MongooseDeleteResult;
    }

    async getNewsForAdmin(): Promise<NewsList> {
        const result = await this.newsModel.find().exec()
            .catch(MongoErrorMatchRpcError)
        if (result) {
            return {
                data: result
            }
        }
        return {
            data: []
        }
    }

    async getNewsForClient({fromPublishDate}: NewsFilter, currentDate): Promise<NewsList> {

        this.newsModel.find({
            $and: [{price: {$ne: 1.99}}, {price: {$exists: true}}]
        })

        const result = await this.newsModel.find({
            $and: [
                {
                    publicationStartDate: {
                        $gte: fromPublishDate
                    },
                },
                {
                    publicationEndDate: {
                        $gte: currentDate
                    },
                },
                {
                    isActive: {
                        $eq: true,
                    }
                }
            ],
        }).exec()
            .catch(MongoErrorMatchRpcError)
        return {
            data: result
        }
    }

    async updateNews(props: News): Promise<News | undefined> {
        return this.newsModel.findByIdAndUpdate(props.id, props, {
            new: true,
        }).exec()
            .catch(MongoErrorMatchRpcError)
    }

    async getNewsById({id}: FilterById): Promise<News | undefined> {
        const result = await this.newsModel.findById(id).exec()
            .catch(MongoErrorMatchRpcError)

        if (result) {
            return result;
        }
        return undefined;
    }

}
