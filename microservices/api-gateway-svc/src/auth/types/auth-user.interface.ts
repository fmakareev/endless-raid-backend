import {User} from "../../_proto_/ts/user";


export interface AuthUserInterface extends User {
    accessToken?: string;
    refreshToken?: string;
}