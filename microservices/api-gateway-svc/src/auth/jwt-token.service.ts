import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

export const JwtAccessTokenServiceName = 'JwtAccessTokenService'

export const JwtAccessTokenService = {
  provide: JwtAccessTokenServiceName,
  inject: [ConfigService],
  useFactory: (configService: ConfigService): JwtService => {
    return new JwtService({
      secret: configService.get<string>('JWT_ACCESS_TOKEN_SECRET'),
      signOptions: {
        audience: configService.get<string>('JWT_AUDIENCE'),
        issuer: configService.get<string>('JWT_ISSUER'),
        expiresIn: configService.get<string>('JWT_EXPIRES_IN'),
      }
    })
  }
}

export const JwtRefreshTokenServiceName = 'JwtRefreshTokenService';
export const JwtRefreshTokenService = {
  provide: JwtRefreshTokenServiceName,
  inject: [ConfigService],
  useFactory: (configService: ConfigService): JwtService => {
    return new JwtService({
      secret: configService.get<string>('JWT_REFRESH_TOKEN_SECRET'),
      signOptions: {
        audience: configService.get<string>('JWT_AUDIENCE'),
        issuer: configService.get<string>('JWT_ISSUER'),
        // expiresIn: configService.get<string>('JWT_EXPIRES_IN'),
      }
    })
  }
}