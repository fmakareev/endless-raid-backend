import { Injectable, ExecutionContext } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { GqlExecutionContext } from '@nestjs/graphql'
import { JwtStrategyName } from './jwt.strategy';

@Injectable()
export class GqlAuthGuard extends AuthGuard(JwtStrategyName) {
  getRequest(context: ExecutionContext): any {
    const ctx = GqlExecutionContext.create(context)
    return ctx.getContext().req
  }
}
