using Colyseus.Schema;

namespace P2PBattleStateSchemaNS
{
	public class UserSchema : Schema
	{
		[Type(0, "string")]
		public string id = default(string);

		[Type(1, "string")]
		public string email = default(string);

		[Type(2, "string")]
		public string userName = default(string);

		[Type(3, "string")]
		public string role = default(string);
	}


	public class PlayerSchema : Schema
	{
		[Type(0, "ref", typeof(UserSchema))]
		public UserSchema user = new UserSchema();

		[Type(1, "boolean")]
		public bool isHost = default(bool);

		[Type(2, "string")]
		public string sessionId = default(string);
	}

	public class P2PBattleRoomState : Schema
	{
		[Type(0, "boolean")]
		public bool isBattleBegun = default(bool);

		[Type(1, "map", typeof(MapSchema<PlayerSchema>))]
		public MapSchema<PlayerSchema> players = new MapSchema<PlayerSchema>();
	}
}