/* eslint-disable */
import { Id, Empty } from './commons';
import { Observable } from 'rxjs';
import { GrpcMethod, GrpcStreamMethod } from '@nestjs/microservices';


export interface News {
  id: string;
  title: string;
  description: string;
  buttonText: string;
  smallPreview: string;
  largePreview: string;
  publishDate: string;
}

export interface NewsList {
  data: News[];
}

export interface NewsServiceClient {

  getNewsById(request: Id, ...rest: any): Observable<News>;

  getNews(request: Empty, ...rest: any): Observable<NewsList>;

  createNews(request: News, ...rest: any): Observable<News>;

  updateNews(request: News, ...rest: any): Observable<News>;

}

export interface NewsServiceController {

  getNewsById(request: Id, ...rest: any): Promise<News> | Observable<News> | News;

  getNews(request: Empty, ...rest: any): Promise<NewsList> | Observable<NewsList> | NewsList;

  createNews(request: News, ...rest: any): Promise<News> | Observable<News> | News;

  updateNews(request: News, ...rest: any): Promise<News> | Observable<News> | News;

}

export function NewsServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ['getNewsById', 'getNews', 'createNews', 'updateNews'];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod('NewsService', method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod('NewsService', method)(constructor.prototype[method], method, descriptor);
    }
  }
}

export const protobufPackage = 'news'

export const NEWS_PACKAGE_NAME = 'news'
export const NEWS_SERVICE_NAME = 'NewsService';