import {Field, InputType} from "@nestjs/graphql";

@InputType()
export class UpdateUserEmailInputType {
  @Field()
  id: string;
  @Field()
  oldEmail: string;
  @Field()
  newEmail: string;
}