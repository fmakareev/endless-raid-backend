import {Field, InputType} from "@nestjs/graphql";

@InputType()
export class NewsFilterInput {
    @Field()
    fromPublishDate: string;
}
