import {Test, TestingModule} from '@nestjs/testing';
import {INestApplication} from '@nestjs/common';
import request from 'supertest';
import {AppModule} from '../src/app.module';

describe('AppController (e2e)', () => {
    let app: INestApplication;

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();

        app = moduleFixture.createNestApplication();
        await app.init();
    });

    it('/auth/login', () => {
        return request(app.getHttpServer())
            .post('/auth/login',)
            .send({login: 'john', password: 'qwerty'})
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Hello World!');
    });
});
