import {Field, InputType} from "@nestjs/graphql";

@InputType()
export class UpdateUserPasswordInputType {
  @Field()
  email: string;
  @Field()
  oldPassword: string;
  @Field()
  newPassword: string;
}