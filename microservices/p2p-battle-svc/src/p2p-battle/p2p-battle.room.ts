import {Room, Client} from 'colyseus';
import {P2PBattleRoomState} from './schema/p2p-battle-state.schema';
import {User} from '../_proto_/ts/user';
import {MessageType} from './types/message.type';
import {ActionTypeEnum} from './enums/action-type.enum';

import {get} from 'lodash';

export class P2PBattleRoom extends Room<P2PBattleRoomState> {

    maxClients = 2;

    onCreate(): void | Promise<any> {
        this.setState(new P2PBattleRoomState());

        this.onMessage(ActionTypeEnum.EndGame, (client: Client, message) => {
            this.broadcast(ActionTypeEnum.EndGame, {
                actionType: ActionTypeEnum.EndGame,
                sessionId: client.sessionId,
                payload: message,
            })
            this.disconnect();
        });

        this.onMessage(ActionTypeEnum.Action, (client: Client, message) => {
            this.broadcast(ActionTypeEnum.Action, {
                actionType: ActionTypeEnum.Action,
                sessionId: client.sessionId,
                payload: message,
            } as MessageType)
        });

        this.onMessage(ActionTypeEnum.ActionResult, (client: Client, message) => {
            this.broadcast(ActionTypeEnum.ActionResult, {
                actionType: ActionTypeEnum.ActionResult,
                sessionId: client.sessionId,
                payload: message,
            } as MessageType)
        });
    }

    onJoin(client: Client, options: { user: User }) {
        console.log('onJoin options: ', options);

        this.state.createPlayer(client.sessionId, get(options, ['user']));
        //
        this.state.setIsBattleBegun(this.maxClients);
    }

}
