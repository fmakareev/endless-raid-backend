import {Field, InputType} from "@nestjs/graphql";

@InputType()
export class LoginInputType {
    @Field()
    password: string;
    @Field()
    email: string;
}