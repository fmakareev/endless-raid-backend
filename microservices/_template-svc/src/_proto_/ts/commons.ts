/* eslint-disable */

export interface Id {
  id: string;
}

export interface Empty {
}

export const protobufPackage = 'commons'

export const COMMONS_PACKAGE_NAME = 'commons'