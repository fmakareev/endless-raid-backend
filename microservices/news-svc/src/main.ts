import { NestFactory } from '@nestjs/core'
import { Logger } from 'nestjs-pino'
import {Transport, MicroserviceOptions, RpcException} from '@nestjs/microservices'
import { join } from 'path'

import { AppModule } from './app.module'
import {ArgumentsHost, Catch, INestMicroservice, RpcExceptionFilter} from '@nestjs/common'
import { NEWS_PACKAGE_NAME } from './_proto_/ts/news'
import {Observable, throwError} from "rxjs";

@Catch(RpcException)
export class MyExceptionFilter implements RpcExceptionFilter<RpcException> {
  catch(exception: RpcException, host: ArgumentsHost): Observable<any> {
    return throwError(exception.getError());
  }
}

async function main() {
  console.log(`${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`);
  const app: INestMicroservice = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
    transport: Transport.GRPC,
    options: {
      url: `${process.env.GRPC_HOST}:${process.env.GRPC_PORT}`,
      package: NEWS_PACKAGE_NAME,
      protoPath: join(__dirname, './_proto_/news.proto'),
      loader: {
        keepCase: true,
        enums: String,
        oneofs: true,
        arrays: true
      }
    }
  })

  app.useLogger(app.get(Logger))
  //app.useGlobalFilters(new MyExceptionFilter())
  return app.listenAsync()
}

void main()
