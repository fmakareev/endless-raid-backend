import { Injectable } from '@nestjs/common';
import {
  CreateGuestDto,
  CreateUserDto,
  FilterByEmail,
  User,
  ChangeUserEmailDto,
  UserList,
  UserPasswordDto,
  UserRole,
  UserServiceController,
  UserUpdateDto,
  UserGuestUpdateDto,
} from '../_proto_/ts/user';
import { FilterById, MongooseDeleteResult } from 'src/_proto_/ts/commons';
import { Document, Model } from 'mongoose';
import { USER_MODEL_NAME } from './schemas/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { MongoErrorMatchRpcErrorHandle, PasswordService } from '@app/common';
import { status } from 'grpc';
import { assignIn, isEmpty } from 'lodash';
import { RpcException } from '@nestjs/microservices';


@Injectable()
export class UserService implements UserServiceController {

  constructor(
    @InjectModel(USER_MODEL_NAME) private readonly userModel: Model<User & Document & { passwordSalt: string }>,
    private readonly passwordService: PasswordService,
  ) {
  }

  createGuestUser(request: CreateGuestDto): Promise<User> {
    return new this.userModel({
      ...request,
      email: null,
      role: UserRole.GUEST,
      userName: `User_${Date.now()}`,
    }).save().catch(MongoErrorMatchRpcErrorHandle);
  }

  async createUser(request: CreateUserDto): Promise<User> {
    try {
      const hash = await this.passwordService.hash(request.password);
      const user = assignIn(request, {
        role: UserRole.PLAYER,
        password: hash,
      });
      return new this.userModel(user).save().catch(MongoErrorMatchRpcErrorHandle);
    } catch (e) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: e,
      });
    }
  }

  async changePassword(request: UserPasswordDto): Promise<User> {
    const hash = await this.passwordService.hash(request.newPassword);

    return this.userModel.findOneAndUpdate({
      email: request.email,
    }, {
      ...request,
      password: hash,
    }, {
      new: true,
    }).exec().catch(MongoErrorMatchRpcErrorHandle);
  }

  async changeEmail(request: ChangeUserEmailDto): Promise<User> {

    const user = await this.userModel.findById(request.id).exec()
      .catch(MongoErrorMatchRpcErrorHandle);
    if (isEmpty(user)) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'Email or id incorrect',
      });
    }
    if (user.email !== request.oldEmail) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'Email incorrect',
      });
    }

    return this.userModel.findByIdAndUpdate(request.id, {
      ...request,
      email: request.newEmail,
    }, {
      new: true,
    });

  }

  async updateGuestUser(request: UserGuestUpdateDto): Promise<User> {
    const user = await this.userModel.findById(request.id).exec()
      .catch(MongoErrorMatchRpcErrorHandle);
    if (isEmpty(user)) {
      throw new RpcException({
        code: status.INVALID_ARGUMENT,
        message: 'Email or id incorrect',
      });
    }
    const hash = await this.passwordService.hash(request.password);

    return this.userModel.findByIdAndUpdate(request.id, {
      ...request,
      email: request.email,
      role: UserRole.PLAYER,
      password: hash,
    }, {
      new: true,
    });
  }

  async deleteUser(request: FilterById): Promise<MongooseDeleteResult> {
    const result: any = await this.userModel.deleteOne({ _id: request.id }).exec()
      .catch(MongoErrorMatchRpcErrorHandle);
    return result as MongooseDeleteResult;
  }

  async getUserById(request: FilterById): Promise<User> {
    const result = await this.userModel.findById(request.id).exec()
      .catch(MongoErrorMatchRpcErrorHandle);

    if (result) {
      return result;
    }
    return undefined;
  }

  async getUserByEmail(request: FilterByEmail): Promise<User> {
    const result = await this.userModel.findOne({
      email: request.email,
    }).exec().catch(MongoErrorMatchRpcErrorHandle);
    if (!isEmpty(result)) {
      return result;
    }
    return null;
  }

  async getUserList(): Promise<UserList> {
    const result = await this.userModel.find().exec()
      .catch(MongoErrorMatchRpcErrorHandle);
    if (result) {
      return {
        data: result,
      };
    }
    return {
      data: [],
    };
  }

  updateUser(request: UserUpdateDto): Promise<User> {
    return this.userModel.findByIdAndUpdate(request.id, request, {
      new: true,
    }).exec().catch(MongoErrorMatchRpcErrorHandle);
  }

}
