import { Schema, type, MapSchema } from '@colyseus/schema';
import { PlayerSchema } from './player.schema';
import { User } from '../../_proto_/ts/user';
import { UserSchema } from './user.schema';

export class P2PBattleRoomState extends Schema {

  @type('boolean')
  isBattleBegun: boolean;

  @type({ map: PlayerSchema })
  players = new MapSchema<PlayerSchema>();

  createPlayer(sessionId: string, user: User) {
    const player = new PlayerSchema();
    const userSchema = new UserSchema();
    userSchema.createUser(user)
    player.user = userSchema;
    player.sessionId = sessionId;
    player.isHost = this.isHost();
    this.players.set(sessionId, player);
  }

  isHost() {
    return this.players.size === 0;
  }

  setIsBattleBegun(maxClients: number){
    if (this.players.size === maxClients)  {
      this.isBattleBegun = true;
    }
  }

}
