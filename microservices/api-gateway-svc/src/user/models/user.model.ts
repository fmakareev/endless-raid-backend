import {Field, ObjectType, registerEnumType} from '@nestjs/graphql';
import {UserRole} from "../../_proto_/ts/user";

registerEnumType(UserRole, {
    name: 'UserRole',
});

@ObjectType()
export class UserModel {
    @Field()
    id: string;

    @Field({
        nullable: true,
    })
    userName: string;

    @Field({
        nullable: true,
    })
    email: string;

    @Field()
    role: string;

    @Field({
        nullable: true,
    })
    createAt: string;

    @Field({
        nullable: true,
    })
    updateAt: string;
}