
# имя микросервиса, долно совпадать с названием его дирректории
SVC_NAME="service name not defined"
PROTOC_GEN_TS_PATH="./node_modules/.bin/protoc-gen-ts"
OUT_DIR="./_proto_/ts"

###############
# CI/CD scripts
###############

# Собрать микросервис
build-svc: check-ci-env
	cd ./microservices/$(SVC_NAME)/ \
	 && npm ci \
	 && npm run build

# собрать микросервис в docker image, важно проверить что в среде в которой вызывается команда установлен docker и docker-compose
build-image: check-ci-env
	cd ./microservices/$(SVC_NAME)/ \
	&& docker-compose --file=docker-compose.prod.yaml build \
	&& docker save $(SVC_NAME):latest | gzip > $(SVC_NAME).tar.gz \

# Отправить архив с образом микросервиса на сервер и запустить
deploy-image: check-ci-env
	apt-get update && apt-get install -y rsync \
	&& ssh-keyscan -H $(SSH_SERVER_IP) >> ~/.ssh/known_hosts \
	&& cd $(BITBUCKET_CLONE_DIR) \
	&& ssh $(SSH_SERVER_USER)@$(SSH_SERVER_IP) "cd /var/www/ && mkdir -p $(BITBUCKET_REPO_SLUG)/microservices/$(SVC_NAME)/" \
	&& rsync -r -v -e ssh . $(SSH_SERVER_USER)@$(SSH_SERVER_IP):/var/www/$(BITBUCKET_REPO_SLUG)/ --delete-before --exclude '.git' --exclude 'node_modules' \
	&& ssh $(SSH_SERVER_USER)@$(SSH_SERVER_IP) \
	"cd /var/www/$(BITBUCKET_REPO_SLUG)/ \
	&& make create-prod-network \
	&& make run-svc-from-image SVC_NAME=$(SVC_NAME)"

build-and-run: build-svc build-image run-svc-from-image

# Запустить микросервис из докер образа на
run-svc-from-image:
	cd ./microservices/$(SVC_NAME)/ \
	&& docker load < $(SVC_NAME).tar.gz \
    && docker-compose -f docker-compose.prod.yaml up -d

# Вывод содержимого переменных пайплайна
check-ci-env:
	echo "Pipeline vars:" \
	echo "SVC_NAME=$(SVC_NAME)" \
	echo "SSH_SERVER_IP=$(SSH_SERVER_IP)" \
	echo "BITBUCKET_CLONE_DIR=$(BITBUCKET_CLONE_DIR)" \
	echo "SSH_SERVER_USER=$(SSH_SERVER_USER)" \
	echo "BITBUCKET_REPO_SLUG=$(BITBUCKET_REPO_SLUG)" \
	echo "=========================================="

# установка зависимостей в каждом сервисе
npm-install:
	cd ./microservices \
	&& (for i in $$(ls -I "_*"); \
       do echo "cd /$$i"; cd "./$$i"; npm i; cd ../; done;)

# Создание докер сети для продакшена
create-prod-network:
	docker network create er-network | true

# Создание докер сети для продакшена
create-dev-network:
	docker network create er-network-dev | true

dc-remove-images:
	@docker rmi -f $(docker images | grep 'none\|not-' | awk "{print \$3}")
dc-remove-containers:
	@docker rm -v $(docker ps -aq -f status=exited)

########
# GRPC #
########

# Гниратор typescript типов из proto buffers
proto-to-ts:
	cd ./_proto_ \
	&& protoc --plugin=../node_modules/.bin/protoc-gen-ts_proto \
		--ts_proto_opt=returnObservable=false \
		--ts_proto_opt=addNestjsRestParameter=true \
		--ts_proto_opt=nestJs=true \
		--ts_proto_out=./ts ./*.proto
