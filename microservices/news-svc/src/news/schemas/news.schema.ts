import * as mongoose from 'mongoose';


export const NewsSchema = new mongoose.Schema({
    title:{
        type: String
    },
    description:{
        type: String
    },
    shortDesc:{
        type: String
    },
    buttonText:{
        type: String
    },
    buttonUrl:{
        type: String
    },
    smallPreview:{
        type: String
    },
    largePreview:{
        type: String
    },
    publicationStartDate:{
        type: Date
    },
    publicationEndDate:{
        type: Date
    },
    isActive:{
        type: Boolean
    },
});