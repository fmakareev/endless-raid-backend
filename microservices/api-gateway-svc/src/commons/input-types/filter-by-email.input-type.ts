import {Field, InputType} from "@nestjs/graphql";

@InputType()
export class FilterByEmailInputType {
    @Field()
    email: string;
}