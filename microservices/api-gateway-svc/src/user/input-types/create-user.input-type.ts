import {Field, InputType} from "@nestjs/graphql";

@InputType()
export class CreateUserInputType {
  @Field()
  password: string;
  @Field()
  email: string;
}