import { Controller, Inject, UsePipes } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import {
  UserServiceController,
  User,
  UserList,
  USER_SERVICE_NAME,
  CreateGuestDto,
  CreateUserDto,
  FilterByEmail,
  UserUpdateDto,
  UserPasswordDto,
  ChangeUserEmailDto,
  UserGuestUpdateDto,
} from '../_proto_/ts/user';
import { FilterById, MongooseDeleteResult } from '../_proto_/ts/commons';
import { UserService } from './user.service';
import { JoiValidationRpcPipe, CreateAtPipe, UpdateAtPipe } from '@app/common';
import { CreateUserValidation } from './validation/create-user.validation';

@Controller()
export class UserController implements UserServiceController {

  constructor(
    @Inject(USER_SERVICE_NAME)
    private readonly userService: UserService,
  ) {
  }

  @GrpcMethod(USER_SERVICE_NAME, 'createUser')
  @UsePipes(
    new JoiValidationRpcPipe(CreateUserValidation),
    new CreateAtPipe(),
  )
  createUser(request: CreateUserDto): Promise<User> {
    return this.userService.createUser(request);
  }

  @GrpcMethod(USER_SERVICE_NAME, 'createGuestUser')
  @UsePipes(new CreateAtPipe())
  createGuestUser(request: CreateGuestDto): Promise<User> {
    return this.userService.createGuestUser(request);
  }

  @GrpcMethod(USER_SERVICE_NAME, 'deleteUser')
  deleteUser(request: FilterById): Promise<MongooseDeleteResult> {
    return this.userService.deleteUser(request);
  }

  @GrpcMethod(USER_SERVICE_NAME, 'getUserById')
  getUserById(request: FilterById): Promise<User> {
    return this.userService.getUserById(request);
  }

  @GrpcMethod(USER_SERVICE_NAME, 'getUserByEmail')
  getUserByEmail(request: FilterByEmail): Promise<User> {
    return this.userService.getUserByEmail(request);
  }

  @GrpcMethod(USER_SERVICE_NAME, 'getUserList')
  getUserList(): Promise<UserList> {
    return this.userService.getUserList();
  }

  @GrpcMethod(USER_SERVICE_NAME, 'updateUser')
  @UsePipes(new UpdateAtPipe())
  updateUser(request: UserUpdateDto): Promise<User> {
    return this.userService.updateUser(request);
  }

  @GrpcMethod(USER_SERVICE_NAME, 'changePassword')
  @UsePipes(new UpdateAtPipe())
  changePassword(request: UserPasswordDto): Promise<User> {
    return this.userService.changePassword(request);
  }

  @GrpcMethod(USER_SERVICE_NAME, 'updateGuestUser')
  @UsePipes(new UpdateAtPipe())
  updateGuestUser(request: UserGuestUpdateDto): Promise<User> {
    return this.userService.updateGuestUser(request);
  }

  @GrpcMethod(USER_SERVICE_NAME, 'changeEmail')
  @UsePipes(new UpdateAtPipe())
  changeEmail(request: ChangeUserEmailDto): Promise<User> {
    return this.userService.changeEmail(request);
  }

}
